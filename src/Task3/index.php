<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\FightArena;
use App\Task3\FightArenaHtmlPresenter;
use App\Task1\Fighter;

$arena = new FightArena();
$presenter = new FightArenaHtmlPresenter();
$presentation = $presenter->present($arena);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Built-in Web Server</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">
    <style>
        body {
            display: flex;
            justify-content: space-evenly;
            flex-wrap: wrap;
            padding: 20px;
        }
        figcaption {
            text-align: center;
            font-family: Roboto;
            font-size: 24px;
            padding: 20px;
        }
        figure {
            height: 350px;
            width: 350px;
        }
        img {
            height: inherit;
            width: auto;
        }
    </style>
</head>
<body>
    <?php echo $presentation; ?>
</body>
</html>
