<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;
use App\Task1\Fighter;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $str = "";
        $arena->populateArena();
        foreach($arena->all() as $fighter) {
            $str .= "<figure>";
            $str .= '<img src="' . $fighter->getImage() . '">';
            $str .= "<figcaption>{$fighter->getName()}: {$fighter->getHealth()}, {$fighter->getAttack()}</figcaption>";
            $str .= "</figure>";
        }
        return $str;
    }
}
