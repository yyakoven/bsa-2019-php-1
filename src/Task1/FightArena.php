<?php

declare(strict_types=1);

namespace App\Task1;

require_once "Fighter.php";

class FightArena
{
    private $fighters = [];

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        $fighters = $this->fighters;
        usort($fighters, function($fighter1, $fighter2) {
            return (($fighter2->getAttack())<=>($fighter1->getAttack()));
        });
        return ($fighters[0]);
    }

    public function mostHealthy(): Fighter
    {
        $fighters = $this->fighters;
        usort($fighters, function($fighter1, $fighter2) {
            return (($fighter2->getHealth())<=>($fighter1->getHealth()));
        });
        return ($fighters[0]);
    }

    public function populateArena() {
        foreach ($this->generateFighters() as $fighter) {
            $this->add($fighter);
        }
    }

    public function all(): array
    {
        return $this->fighters;
    }

    public static function generateFighters() {
        $data = [
            [
                1,
                'Ryu',
                100,
                10,
                'https://bit.ly/2E5Pouh'
            ],
            [
                2,
                'Chun-Li',
                70,
                30,
                'https://bit.ly/2Vie3lf'
            ],
            [
                3,
                'Ken Masters',
                80,
                20,
                'https://bit.ly/2VZ2tQd'
            ]
        ];
        foreach($data as $d) {
            [$id, $name, $health, $attack, $image] = $d;
            yield (new Fighter($id, $name, $health, $attack, $image));
        }
    }
}
